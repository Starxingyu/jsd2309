package Homework.ooday01;

public class Student {

    String name, className, stuId;
    int age;


    public Student() {
    }

    public Student(String name, String className, String stuId, int age) {
        this.name = name;
        this.className = className;
        this.stuId = stuId;
        this.age = age;
    }

    void study(){
        System.out.println(this.name + "在学习");
    }

    void sayHi(){
        System.out.println("大家好我叫" + this.name + ",今年," + this.age + "岁了,所在班级为"+this.className+",学号为:" + this.stuId);
    }

    void palyWith(String anotherName ){
        System.out.println(this.name + "正在和"+anotherName +"一起玩....");
    }













}

package Homework.ooday01;

public class Car {

    String brand, color;
    int price;

    public Car() {
    }

    public Car(String brand, String color, int price) {
        this.brand = brand;
        this.color = color;
        this.price = price;
    }

    void  print(String state) {
        System.out.println(this.brand + "牌子的" + this.color + "颜色的车" + this.price + state);
    }

    void start() {
        print("启动了");
    }

    void run() {
        print("跑起来了");
    }

    void stop() {
        print("停止了");
    }

}

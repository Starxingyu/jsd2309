package Homework.ooday02;

public class Fish extends Animal {

    public Fish() {
    }

    public Fish(String name, int age, String color) {
        super(name, age, color);
    }

    void eat(){
        System.out.println(color + "颜色的" + age+"岁的" +name  +  "正在吃小虾" );
    }


}

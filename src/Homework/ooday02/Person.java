package Homework.ooday02;

public class Person {


    String name, address;
    int age;

    public Person() {
    }

    public Person(String name, String address, int age) {
        this.name = name;
        this.address = address;
        this.age = age;
    }


    void eat() {
        System.out.println(name + "正在吃饭");
    }


    void sleep() {
        System.out.println(name + "正在睡觉");
    }

    void sayHi() {
        System.out.println("大家好，我叫" + name + "，今年" + age + "岁了，家住" + address);
    }


}

package Homework.ooday02;



public class PerrsonTest {
    public static void main(String[] args) {

        Student stu = new Student();
        stu.name = "老王";
        stu.address = "佳丽斯";
        stu.age = 23;
        stu.classname = "jsd2309";
        stu.stuId = "001";
        stu.eat();
        stu.sleep();
        stu.sayHi();
        stu.study();

        Student ll = new Student("李林","佳木斯",22,"jsd2309","002");
        ll.eat();
        ll.sleep();
        ll.sayHi();
        ll.study();

        Teacher zl = new Teacher("赵强","山东",36,6000.0);
        zl.eat();
        zl.sleep();
        zl.sayHi();
        zl.teach();

        Doctor sx = new Doctor("孙旭","山西",45,"主任医师");
        sx.eat();
        sx.sleep();
        sx.sayHi();
        sx.cut();

        Person p = new Person();
        p.name = "人";
        p.age = 1;
        p.address = "未知";
        p.eat();
        p.sleep();
        p.sayHi();



    }
}

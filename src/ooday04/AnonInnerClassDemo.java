package ooday04;

public class AnonInnerClassDemo {
    public static void main(String[] args) {
    Inter o1 = new Inter() {};

    InterInter o2 = new InterInter() {

        public void show(){
            System.out.println(123);
        }

    };



    }
}

interface Inter{

}

interface InterInter{
    void show();
}




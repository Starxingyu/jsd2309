package ooday04;

public class Chick extends Animal {

    public Chick() {
    }

    public Chick(String name, int age, String color) {
        super(name, age, color);
    }

    void layEggs(){
        System.out.println(color + "颜色的" + age+"岁的" +name  +  "正在下蛋" );
    }

    void eat(){
        System.out.println(color + "颜色的" + age+"岁的" +name  +  "正在吃小米" );
    }


}

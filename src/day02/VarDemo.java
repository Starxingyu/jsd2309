package day02;

//变量的演示
public class VarDemo {
    public static void main(String[] args) {
        //1)变量的声明:--相当于在银行开了个账户
        int a;//声明一个整型的变量,名为a

        int b, c, d;//声明三个整数型变量,名为bcd
        // int a;//编译错误, 变量不能同名

        //2)变量的初始化:--相当于给账户存钱
        int e = 250;//声明整数型变量e并赋值为250 --开户同时存钱
        int f;//声明整型变量f ---- 先开户
        f = 250;//给变量f赋值为250-----后存钱
        f = 360;//修改变量f的值为360
        int g = 5, h = 8, i = 10;//声明三个整型变量g,h,i ,并分别赋值为5,8,10

        //   System.out.println(a);

        //3)变量的使用: ---使用的是账户里面的钱
        int j = 5;//声明整型变量j并赋值5
        int k = j + 10;//取出j的值5,加10后,再赋值给变量k
        //输出是若不加双引号,咋java认为他就是一个变量
        System.out.println(k);//输出变量k的值为15
        j = j + 10;
        //在j的本身基础上增10
        System.out.println(j);//15
        int balance = 5000;//账户余额5000
        balance = balance - 1000;//取款1000
        System.out.println(balance);//4000
//        System.out.println(m);
        int m;
//        System.out.println(m);//编译错误，变量未初始化

        //4)变量的命名:
        int a1, a_5$, _3c, $5b;
//        int a*b;//编译错误,不能包含*号
//        int 1a ;//编译错误,不能以数字开头
        int aa = 5;
//        System.out.println(aA);//编译错误,严格区分大小写
        //int class;//编译错误,不能使用关键词


//        int 年龄;//正确,但不建议
//        int nianling;//必须杜绝
        int age;//建议"英文的见名知义"
        int score, myScore, myJavaScore;//建议"小驼峰命名法:
















































    }
}

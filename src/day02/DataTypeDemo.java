package day02;

import com.sun.java.swing.plaf.windows.WindowsTabbedPaneUI;

import java.math.BigDecimal;

//基本数据类型 的演示
public class DataTypeDemo {
    public static void main(String[] args) {


















        /*
        //5)字符型  两个字节
        char c1 = '女';
        char c2 = 'f';
        char c3 = '6';
        char c4 = '*';
//        char c5  = 女;//编译错误,字符型直接量必须放在单引号中
//        char c6  = '';//编译错误,必须有字符
//        char c7  = '10';//编译错误,只能存储1个字符
        char c8 = 65;
        System.out.println(c8);//A,println()会依据变量的类型做输出演示
                                //c8为char型,所以会以字符的形式输出
        char c9 = '\\';
        System.out.println(c9);
*/

















/*
        //4)boolean 布尔型,一个字节
        boolean a = true;//true为布尔型直接量
        boolean b = false;//false 为 布尔型直接量
//        boolean c = 2520;

  */



















    /*


        //3)double:浮点型,八个字节
        double a = 3.14;//3.14为小数直接量默认为double型
        float b = 3.14F;//3.14F为ffloat直接量

        double c = 6,d=1.2;
        System.out.println(c-d);


   */







  /*
        //2)long :长整型,8个字节,-2^63到2^63-1(正负900万万亿多)
        long a = 25l;//25L为长整型直接量
        //long b  = 100000000000;//编译错误,没有l默认为int类型
        long c = 10000000000l;//100亿L为长整数直接量
        //long d = 3.14;//编译错误,长整型变量中只能装整数
        long e = 1000000000 * 2 * 10L;
        System.out.println(e);
        long f = 1000000000 * 3 * 10L;
        System.out.println(f);
        long g = 1000000000L * 2 * 10;
        System.out.println(g);
*/













        /*
        //1)int:整型,四个字节,-2^32到2^31-1(正负21个多亿)
        int a = 25;
//        int b = 10000000000;//编译错误,100亿默认为int类型,但超出范围了
//        int c = 3.14;//编译错误,整型变量中只能装整数

        System.out.println(5 / 2);//2
        System.out.println(2 / 5);//0
        System.out.println(5 / 2.0);//2.5

        int d = 2147483647;//int的 最大值
        d = d + 1;
        System.out.println(d);
*/


    }
}

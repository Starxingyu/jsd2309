package day02;

public class DataTypeCastDemo {
    public static void main(String[] args) { 
/*
        int a = 5;
        long  b = a;
        int c = (int)b;
        long d = 5;
        double e = 5;
        System.out.println(e);
        long f = 10000000000L;
        int g = (int)f;
        System.out.println(g);
        double h = 25.987;
        int i = (int)h;
        System.out.println(i);
*/
        byte b1 = 5;
        byte b2 = 6;
//        byte b3 = b1 +b2;//编译错误
//        byte b3 = (byte)b1 +b2;//编译错误 ,优先级高
        byte b3 = (byte)(b1 +b2);
        System.out.println(2+2);
        System.out.println(2+'2');
        System.out.println('2'+'2');
        System.out.println('我'+'你');
        System.out.println((int)'我');
        System.out.println('2');


        int  m ='a';
        char n = 97;
        System.out.println(m);
        System.out.println(n);





    }
}

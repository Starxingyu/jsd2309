package ooday01;

public class Car {

    String brand, color;
    double price;


    public Car() {
    }

    public Car(String brand, String color, double price) {
        this.brand = brand;
        this.color = color;
        this.price = price;
    }


    void start() {

        print("启动了");
    }

    void run() {

        print("开始跑了");
    }

    void stop() {

        print("停止了");
    }

    void print(String state) {
        System.out.println(this.brand + "牌子的" + this.color + "颜色的车" + this.price + state);
    }


}


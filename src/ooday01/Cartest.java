package ooday01;

public class Cartest {
    public static void main(String[] args) {

        Car car = new Car("大众", "黑", 35500.00);
        car.start();
        car.run();
        car.stop();

        Car ca = new Car();
        ca.color = "白";
        ca.brand = "宝马";
        ca.price = 350000.00;
        ca.start();
        ca.run();
        ca.stop();

        Car cara = new Car("奔驰", "黑", 355300.00);
        cara.start();
        cara.run();
        cara.stop();

    }
}

package ooday01;

import javax.xml.stream.events.StartDocument;

public class Student {

    String name;
    int age ;
    String className;
    String stuId;

    public Student() {
    }

    public Student(String name, int age, String className, String stuId) {
        this.name = name;
        this.age = age;
        this.className = className;
        this.stuId = stuId;
    }


    void study(){
        System.out.println(this.name + "在学习");
    }

    void sayHi(){
        System.out.println("大家好我叫" + this.name + ",今年," + this.age + "岁了,所在班级为"+this.className+",学号为:" + this.stuId);
    }

    void palyWith(String anotherName ){
        System.out.println(this.name + "正在和"+anotherName +"一起玩....");
    }

}

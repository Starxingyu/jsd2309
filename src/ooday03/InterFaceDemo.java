package ooday03;

public class InterFaceDemo {

    public static void main(String[] args) {

//        Inter o = new Inter()


    }
}

//演示接口的语法:
interface Inter {
    abstract void show();

    void Test();
}

class interImpl implements Inter {
    public void show() {

    }
    @Override
    public void Test() {

    }
}


interface Inter1{
    void show();
}

interface Inter2{
    void Test();
}
abstract class Aoo{
    abstract void say();
}


class Boo extends Aoo implements Inter1,Inter2{
    @Override
    public void show() {

    }
    @Override
    public void Test() {

    }
    @Override
    void say() {

    }

    interface Inter3{
        void show();
    }


    interface Inter4 extends Inter3{
        void test();
    }


    class Coo implements Inter4{

        @Override
        public void test() {

        }

        @Override
        public void show() {

        }
    }

}






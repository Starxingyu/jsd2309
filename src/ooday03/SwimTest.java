package ooday03;

public class SwimTest {
    public static void main(String[] args) {

        Dog dog = new Dog("小黑", 2, "黑");
        dog.eat();
        dog.drink();
        dog.lookhome();
        dog.swim();

        Chick chick = new Chick("小黑", 2, "黑");
        chick.eat();
        chick.drink();
        chick.layEggs();


        Fish fish = new Fish("小黑", 2, "黑");
        fish.eat();
        fish.drink();
        fish.swim();

    }
}

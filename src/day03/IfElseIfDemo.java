package day03;

import java.util.Scanner;

public class IfElseIfDemo {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.print("请输入商品价格:");
        double num = scan.nextDouble();

        if (num >= 2000) {
            num *= 0.5;
        } else if (num >= 1000) {
            num *= 0.7;
        } else if (num >= 500) {
            num *= 0.8;
        } else if (num > 0) {
            num *= 0.9;
        } else {
            System.out.println("输入错误程序终止");
            return;
        }
        System.out.println("消费金额为" + num + "元");
    }

}

package day03;

public class IfElseDemo {
    public static void main(String[] args) {

        //1)
        double num = 320.0;
        if (num >= 500) {
            num *= 0.8;
        } else if (num < 500 && num > 0) {
            num *= 0.9;
        } else {
            System.out.println("输入错误程序终止");
            return;
        }

        System.out.println("消费金额为" + num + "元");

        int score = 101;

        if (score >= 0 && score <= 100) {
            System.out.println(score + "成绩是合法的");
        } else {
            System.out.println(score + "成绩不合法");
        }


    }
}

package day03;

import java.util.Scanner;

public class IfDemo {
    public static void main(String[] args) {
//        Scanner scan = new Scanner(System.in);
//        System.out.print("请输入商品价格:");
//        double num = scan.nextDouble();
//        if (num >= 1000 ){
//            num *=0.7;
//        }else if(num < 1000 && num >=500){
//            num *=0.8;
//        } else if (num <500 && num >0) {
//            num *=0.9;
//        }else {
//            System.out.println("输入错误程序终止");
//            return;
//        }
//        System.out.println("消费金额为" + num + "元");


        //1）满500打八折

        double price = 99900.0;
        if (price >= 500) {
            price *= 0.8;
            System.out.println("消费金额为:" + price);
        }

        //2）判断成绩是否合法
        int score = 995;
        if (score >= 0 && score <= 100) {
            System.out.println(score + "成绩是合法的");
        } else {
            System.out.println("成绩不合法");
        }


    }
}

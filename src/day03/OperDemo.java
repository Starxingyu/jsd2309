package day03;

//运算符的演示

import java.util.Scanner;

public class OperDemo {
    public static void main(String[] args) {
        //6.条件运算符: boolean?数1: 数2
       /*
        int num = 5;
        int flag = num > 0 ? 1 : -1;
        System.out.println(flag);
        int  a  =8 ,b=5;
        int  max = a>b?a:b;
        System.out.println("max=" + max);
        */



/*
        int age = 39;
        System.out.println("age=" + age);
        System.out.println("我今年" + age + "岁了");

        String name = "wkj";
        System.out.println("大家好我叫" + name);
        System.out.println("大家好我叫" + name + ",今年" + age + "岁了");

        System.out.println(10 + 20 + "" + 30);
        System.out.println("" + 10 + 20 + 30);
        System.out.println(10 + 20 + 30 + "");
*/



       /*
        int a=5,b=10,c=5;
        a += 10;
        System.out.println(a);
        a *=2;
        System.out.println(a);
        a /= 6;
        System.out.println(a);

        short s  = 5;
        s = (short)(s + 10);
        s += 10;
        System.out.println(s);
*/









/*
        boolean b3 = a>b && c++>2;
        System.out.println(b3);//false
        System.out.println(c);//5,发生短路了


        boolean b4 = a>b || c++>2;
        System.out.println(b4);//true
        System.out.println(c);//5,发生短路了
*/


        /*
        int a=5,b=10,c=5;
        System.out.println(b>=a || b<c);
        System.out.println(b<=c || b>a);
        System.out.println(b!=c || a<b);
        System.out.println(a==b || b<c);
        int score = 98;
        System.out.println(score<0 || score>100);

        boolean b2 = !(a<b);
        System.out.println(b2);
        System.out.println(!(a>b));
        */


        /**
         * &&:短路与(并且)，两边都为真则为真，见false则false
         * ||短路或(或者)，有真则为真，见true则true
         * !:逻辑非(取反)，非真则假，非假则真
         */
 /*
        int a = 5 , b = 10, c = 5;
        boolean b1 = b>=a && b<c;
        System.out.println(b1);
        System.out.println(b<=c && b>a);
        System.out.println(a==b && c>b);
        System.out.println(b!=c && a<b);
        int age =99;
        System.out.println(age>=18 && age<=50);
        int score = 86;
        System.out.println(score>=0 && score<=100);

*/






















/*
        int a = 5, b = 10, c = 5;
        boolean b1 = a>b;
        System.out.println(b1);
        System.out.println(c<b);
        System.out.println(a>=c);
        System.out.println(a<=b);
        System.out.println(a==c);
        System.out.println(a!=c);
        System.out.println(a%2==0);
        System.out.println(a+c>b);
        System.out.println(a++>5);
        System.out.println(a++>5);

*/

        //1.算数运算符,+,-,*,/,++,--,
/*
        int a = 5, b = 5;
        a++;//相当于a = a+1;

        ++b;//相当于b = b+1;
        System.out.println(a);
        System.out.println(b);
*/
        /*
        int a = 5, b = 5; //a+的值为0，++a的值为a+1
        int c = a++;//将a++的值5赋值给c,同时a自增1变为6
        int d = ++b; //将++b的值6赋值给d,同时b自增1变为6
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);
*/
/*
        int a = 5, b = 5; //a的值为5，--a的值为a-1
        int c = a--;//将a--的值5赋值给c,同时a自减1变为4
        int d = --b; //将--b的值6赋值给d,同时b自减1变为4
        System.out.println(a);//4
        System.out.println(b);//4
        System.out.println(c);//5
        System.out.println(d);//4

*/


    }
}

package day04;

import java.util.EnumMap;

public class ForDemo {
    public static void main(String[] args) {

//        for(int times=0;times<5;times++){
//            System.out.println("行动是成功的阶梯");
//        }
//        System.out.println("继续执行...");
//
//        for (int num = 1;num<=9;num++){
//            System.out.print(num + "*9=" + num*9 + "   " );
//        }


        for (int a = 1; a <= 9; a++) {
            System.out.println();
            for (int num = 1; num <= a; num++) {
                System.out.print(num + "*" + a + "=" + num * a + "   ");
            }
        }
        System.out.println();
        int sum = 0;
        for (int i = 1; i <= 100; i++) {
            sum += i;

        }

        System.out.println("sum=" + sum);

        for(int num = 1;num<=9;num++){
            if (num%3==0){
                continue;
            }
            System.out.println(num + "*9=" + num *9);
        }


        for(int num = 1;num<=9;num++){
            if (num%3==0){
                break;
            }
            System.out.println(num + "*9=" + num *9);
        }

    }
}

package day04;

import java.util.Scanner;

public class CommandBySwich {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        for (int i = 0; true; i++) {
            System.out.println("请选择您需要的服务1.存款 2.取款 3.查询余额 4.退卡");
            int command = scan.nextInt();
            switch (command) {
                case 1:
                    System.out.println("感谢您选择存款业务");
                    return;
                case 2:
                    System.out.println("感谢您选择取款业务");
                    return;
                case 3:
                    System.out.println("感谢您选择查询余额业务");
                    return;
                case 4:
                    System.out.println("好的,欢迎下次使用");
                    return;
//                case 5:
//                    System.out.println(555);
//                    return;
//                case 6:
//                    System.out.println(666);
//                    return;
//                case 7:
//                    System.out.println(777);
//                    return;
//                case 8:
//                    System.out.println(888);
//                    return;
//                case 9:
//                    System.out.println(999);
//
//                case 0:
//                    System.out.println(0);
//                    return;
                default:
                    if (i == 5) {
                        System.out.println("选择多次错误,程序终止");
                        return;
                    } else {
                        System.out.println("输入错误请重新输入");
                    }
            }
        }






    }
}

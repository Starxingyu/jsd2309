package day01;
//声明包day01


public class HelloWorld {
    //声明类HelloWorld
    public static void main(String[] args) {
        //主方法,为程序的入口(大门口),运行程序时自动执行main方法

        /**
         * 1)严格区分大小写
         * 2)所有符号必须是英文模式的
         * 3)每句话必须以 分号结尾
         */
        System.out.println("hello world");
        System.out.println("欢迎大家来到达内");
        System.out.println(25);
    }
}

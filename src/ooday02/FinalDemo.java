package ooday02;

/**
 * final的演示
 */
public class FinalDemo {
}


//演示final修饰变量
class Eoo {
    final int a = 5;
    int b = 6;

    void test() {
        //a = 55; //编译错误，final修饰的变量不能被改变
        b = 66;
        final int c = 8;
        //c = 88; //编译错误，final修饰的变量不能被改变
    }
}

//演示final修饰方法
class Foo {
    final void show() {
    }

    void test() {
    }
}

class Goo extends Foo {
    //void show(){} //编译错误，final修饰的方法不能被重写
    void test() {
    }
}


final class Hoo {
}

class Joo {
}

final class Koo extends Joo {
}












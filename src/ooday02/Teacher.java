package ooday02;

public class Teacher extends Person{
    double salary;

    public Teacher() {
    }

    Teacher(String name,int age,String address,double salary){
        this.name = name;
        this.age = age;
        this.address = address;
        this.salary = salary;
    }

    void teach(){
        System.out.println(name+"正在讲课...");
    }
    void sayHi(){
        System.out.println("大家好，我叫"+name+"，今年"+age+"岁了，家住"+address + "工资是" + salary);
    }
}
